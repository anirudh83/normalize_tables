[Task: ](https://learn.mountblue.io/targets/1268) **Database Design Drills**
<h1> Normalize Each Of The Following Tables</h1>
<h3>Task 1:  BRANCH</h3>
<ul>
<li>Branch#</li>
<li>Branch_Addr</li>
<li>ISBN</li>
<li>Title</li>
<li>Author</li>
<li>Publisher</li>
<li>Num_copies</li>
</ul>
<h3>Task 2:  CLIENT</h3>
<ul>
<li>Client#</li>
<li>Name</li>
<li>Location</li>
<li>Manager#</li>
<li>Manager_name</li>
<li>Manager_location</li>
<li>Contract#</li>
<li>Estimated_cost</li>
<li>Completion_date</li>
<li>Staff#</li>
<li>Staff_name</li>
<li>Staff_location</li>
</ul>

<h3>Task 3: PATIENT</h3>
<ul>
<li>Patient#</li>
<li>Name</li>
<li>DOB</li>
<li>Address</li>
<li>Prescription#</li>
<li>Drug</li>
<li>Date</li>
<li>Dosage</li>
<li>Doctor</li>
<li>Secretary</li>
</ul>

<h3>Task 4 :DOCTOR</h3>
<ul>
<li>Doctor#</li>
<li>DoctorName</li>
<li>Secretary</li>
<li>Patient#</li>
<li>PatientName</li>
<li>PatientDOB</li>
<li>PatientAddress</li>
<li>Prescription#</li>
<li>Drug</li>
<li>Date</li>
<li>Dosage</li>
</ul>