CREATE TABLE IF NOT EXISTS manager(
    manager_id INT NOT NULL,
    manager_name VARCHAR(255) NOT NULL,
    manager_location VARCHAR(255),
    PRIMARY KEY (manager_id)
);

CREATE TABLE IF NOT EXISTS contract(
    contract_id INT NOT NULL,
    estimated_cost INT NOT NULL,
    completion_date date,
    PRIMARY KEY (contract_id)
);

CREATE TABLE IF NOT EXISTS staff(
   staff_id INT NOT NULL,
   staff_name VARCHAR(255) NOT NULL,
   staff_location text,
   PRIMARY KEY (staff_id)
);

CREATE TABLE IF NOT EXISTS clients(
    client_id INT NOT NULL,
    client_name VARCHAR(255) NOT NULL,
    location VARCHAR(255),
    staff_id INT,
    contract_id INT,
    manager_id INT,
    PRIMARY KEY (client_id),
    FOREIGN KEY (staff_id) REFERENCES staff(staff_id),
    FOREIGN KEY (contract_id) REFERENCES contract(contract_id),
    FOREIGN KEY (manager_id) REFERENCES manager(manager_id)
);
