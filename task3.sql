CREATE TABLE IF NOT EXISTS patient (
    patient_id INT NOT NULL,
    name VARCHAR(255) NOT NULL,
    dob DATE,
    address  text,
    PRIMARY KEY (patient_id)
);

CREATE TABLE IF NOT EXISTS doctor (
    doctor_id INT AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    secretary VARCHAR(255),
    PRIMARY KEY (doctor_id)
);

CREATE TABLE IF NOT EXISTS prescription (
    prescription_id INT NOT NULL,
    drug VARCHAR(255) NOT NULL,
    date DATE,
    dosage INT NOT NULL,
    doctor_id INT,
    patient_id INT NOT NULL,
    PRIMARY KEY (prescription_id),
    FOREIGN KEY (doctor_id) REFERENCES doctor(doctor_id),
    FOREIGN KEY (patient_id) REFERENCES patient(patient_id)
);