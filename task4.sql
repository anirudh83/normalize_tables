CREATE TABLE IF NOT EXISTS doctor_task4 (
    doctor_id INT NOT NULL,
    doctor_name VARCHAR(255),
    secretary VARCHAR(255),
    PRIMARY KEY (doctor_id)
);

CREATE TABLE IF NOT EXISTS patient_task4 (
    patient_id INT NOT NULL,
    patient_name VARCHAR(255) NOT NULL,
    patient_dob DATE,
    patient_address  text,
    PRIMARY KEY (patient_id)
);

CREATE TABLE IF NOT EXISTS prescription_task4 (
    prescription_id INT NOT NULL,
    drug VARCHAR(255) NOT NULL,
    date DATE,
    dosage INT NOT NULL,
    doctor_id INT,
    patient_id INT NOT NULL,
    PRIMARY KEY (prescription_id),
    FOREIGN KEY (doctor_id) REFERENCES doctor_task4(doctor_id),
    FOREIGN KEY (patient_id) REFERENCES patient_task4(patient_id)
);